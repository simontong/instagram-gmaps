import {bootstrap}    from 'angular2/platform/browser';
import {HTTP_PROVIDERS} from 'angular2/http';
import {ImageComponent} from './images.component';

bootstrap(ImageComponent, [HTTP_PROVIDERS]);