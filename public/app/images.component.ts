/// <reference path='../../typings/tsd.d.ts' />

import {Component} from "angular2/core";
import {Http} from 'angular2/http';
import {CORE_DIRECTIVES, FORM_DIRECTIVES} from "angular2/common"

class Image {
    id: Number;
    url: String;
    caption: String;
    niceTime: String;
    userImageUrl: String;
    username: String;
    likeCount: Number;
    commentCount: Number;
}

@Component({
    selector: "images",
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES],
    template: `
        <div class="ui basic modal" id="image-enlarge">
            <i class="close icon"></i>
            <div class="header">
                &nbsp;
            </div>
            <div class="image content">
                <div class="image">
                    <img src="" alt="" />
                </div>
                <div class="description">
                    <p></p>
                </div>
            </div>
            <!--<div class="actions">
                <div class="two fluid ui inverted buttons">
                    <div class="ui red basic inverted button">
                        <i class="remove icon"></i>
                        No
                    </div>
                    <div class="ui green basic inverted button">
                        <i class="checkmark icon"></i>
                        Yes
                    </div>
                </div>
            </div>-->
        </div>
        <div class="ui three stackable cards">
            <div *ngFor="#image of images" class="ui card">
                <div class="content">
                    <div class="right floated meta">{{image.niceTime}}</div>
                    <img class="ui avatar image" src="{{image.userImageUrl}}"> {{image.username}}
                </div>
                <div class="image">
                    <img src="{{image.url}}" alt="" (click)="clickImage(image)" />
                </div>
                <div class="content">
                    <span class="right floated">
                        <i class="heart outline like icon"></i> {{image.likeCount}} likes
                    </span>
                    <i class="comment icon"></i> {{image.commentCount}} comments
                </div>
                <!--<div class="extra content">
                    <div class="ui large transparent left icon input">
                        <i class="heart outline icon"></i>
                        <input type="text" placeholder="Add Comment...">
                    </div>
                </div>-->
            </div>
        </div>
    `
})
export class ImageComponent {

    public images: Array<Image> = [];

    constructor(public http: Http) {
        var that = this,
            image;

        http.get('/images').subscribe(res => {
            for (let img of res.json()) {
                console.log(img);
                image = new Image;
                image.id = img.id;
                image.url = img.images.standard_resolution.url;
                image.caption = img.caption.text;
                image.niceTime = img.created_time;
                image.userImageUrl = img.user.profile_picture;
                image.username = img.user.username;
                image.likeCount = img.likes.count;
                image.commentCount = img.comments.count;

                that.images.push(image);
            }
        });

    }

    static clickImage(image: Image) {
        var $img = $('#image-enlarge');
        $img.find('.image.content img').attr('src', image.url);
        $img.find('.description p').html(image.caption);
        $img.modal('show');
    }
}