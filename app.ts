/// <reference path='./typings/tsd.d.ts' />

import express = require('express');
import favicon = require('serve-favicon');
import logger = require('morgan');
import cookieParser = require('cookie-parser');
import bodyParser = require('body-parser');

var app = express();
app.set('env', 'development');

// view engine
app.set('view engine', 'ejs');

app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// routes
app.use(express.static(__dirname + '/public'));

// node modules to use
var nodeMods = [
    'angular2', 'es6-shim', 'jquery', 'reflect-metadata', 'rxjs', 'semantic-ui',
    'systemjs', 'typescript'
];
for (var i = 0; i < nodeMods.length; i++) {
    app.use('/' + nodeMods[i], express.static(__dirname + '/node_modules/' + nodeMods[i]));
}

app.get('/', (req, res) => {
    res.render('layout');
});

app.get('/images', (req, res) => {

    var ig = require('instagram-node').instagram();
    ig.use({
        client_id: '9224ba7f307346eb8f270f3e4b7ce2ee',
        client_secret: 'c23761c81a994b6fa52782e8eb9d00be'
    });

    //ig.use({ access_token: '431161945.9224ba7.6ca4d8653130490f80442f10873cfd57' });
    ig.use({ access_token: '431161945.1fb234f.d557582893344c819fe6719cfa0803af' });

    /*var redirect_uri = 'http://localhost:3000/images';
    if (req.query.code) {
        ig.authorize_user(req.query.code, redirect_uri, function(err, result) {
            if (err) {
                console.log(err.body);
                res.send("Didn't work");
            } else {
                //console.log('Yay! Access token is ' + result.access_token);
                res.send('You made it!! - ' + result.access_token);
            }
        });

        return;
    }

    res.send(ig.get_authorization_url(redirect_uri, { scope: ['likes', 'public_content'], state: 'a state' }));
    return;*/

    ig.media_search(51.5, -0.13, (err, medias, remaining, limit) => {

        res.json(medias);

    });



});

//catch 404 and forward to error handler
app.use((req, res, next) => {
    var err = new Error('Not Found');
    err['status'] = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use((err: any, req, res, next) => {
        res.status(err['status'] || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use((err: any, req, res, next) => {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

export = app;
